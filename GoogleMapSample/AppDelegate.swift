//
//  AppDelegate.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/07/30.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import GoogleMaps
import NCMB

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    //mBaaS
    //セキュリティのため非表示
    let applicationkey = "xxxxx"
    let clientkey = "xxxxx"

    //全画面共有のユーザー情報
    //ユーザー構造体
    var id: String?
    var userName: String?
    var thumbnailURL: String?
    var accessToken: String?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //セキュリティのため非表示
        GMSServices.provideAPIKey("xxxxx")
        //GMSPlacesClient.provideAPIKey("AIzaSyBLIKTuJF5HWpZztHW4YP9iEt5XDzdT")

        //初期化
        NCMB.setApplicationKey(applicationkey, clientKey: clientkey)
        var currentuser:String? = nil

        if UserDefaults.standard.object(forKey: "ID") != nil {
            print(UserDefaults.standard.object(forKey: "ID") as! String)
            currentuser = UserDefaults.standard.object(forKey: "ID") as? String
        }

        //ユーザーがいない場合サインイン画面に遷移
        if (currentuser == nil){
            //windowを生成
            self.window = UIWindow(frame: UIScreen.main.bounds)
            //Storyboardを指定
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "login")
            //rootViewControllerに入れる
            self.window?.rootViewController = initialViewController
            //表示
            self.window?.makeKeyAndVisible()
        }else{
            self.id = currentuser
            self.userName = UserDefaults.standard.object(forKey: "USERNAME") as? String
            self.thumbnailURL = UserDefaults.standard.object(forKey: "THUMBNAILURL") as? String
            self.accessToken = UserDefaults.standard.object(forKey: "ACCESSTOKEN") as? String

            //ユーザーがいる場合Storyboardでチェックの入っているIs Initial View Controllerに遷移する
            self.window = UIWindow(frame: UIScreen.main.bounds)
            //Storyboardを指定
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "home")
            //rootViewControllerに入れる
            self.window?.rootViewController = initialViewController
            //表示
            self.window?.makeKeyAndVisible()

        }

        return true
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

