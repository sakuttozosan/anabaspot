//
//  DetailViewController.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/08/09.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import Haneke
import GoogleMaps
import Alamofire
import SwiftyJSON
import NCMB

class DetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {


    var googleMap: GMSMapView!

    @IBOutlet weak var detailImgView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var headerImgHeight: NSLayoutConstraint!
    @IBOutlet weak var mapDisplayView: UIView!

    @IBOutlet weak var favoriteBtn: UIImageView!

    //お気に入りかどうか
    var isFavorite: Bool = false
    //データベース
    let obj = NCMBObject(className: "userFavorite")

    //仮データベース
    var objTemp: [NCMBObject] = []

    //    @IBOutlet weak var detailImgView: UIImageView!

    //    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
//    @IBOutlet weak var headerImgHeight: NSLayoutConstraint!

//    @IBOutlet weak var mapDisplayView: UIView!

//    @IBOutlet weak var headerSpotLabel: UILabel!
//    @IBOutlet weak var headerSearchingLabel: UILabel!

    var appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
//    let accessToken: String = "1591420171.b8d896e.42bff6e6c6984e9cbd6f57dfcf8711f6"
    var accessToken: String = "1591420171.b8d896e.42bff6e6c6984e9cbd6f57dfcf8711f6"
    //セキュリティのため非表示
    let param = ["client_id": "xxxxx"]

    var imgURL: URL?
    var imgLat: Double?
    var imgLon: Double?
    var imgName: String?

    var imgSearchSpotName:String?

    var urlString: String = ""
    var encodedUrl: String = ""

    var cellSize:CGFloat!

    let nc = NotificationCenter.default

    let myNotification = Notification.Name(rawValue: "setHeight")
    let dbNotification = Notification.Name(rawValue: "dbOperate")


    var sameSpots:[NSURL] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        accessToken = appDelegate.accessToken!

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFavorite))
        favoriteBtn.addGestureRecognizer(tap)
        // 角丸にする
        favoriteBtn.layer.cornerRadius = favoriteBtn.frame.size.width * 0.2
        favoriteBtn.clipsToBounds = true

        //初期表示
        self.initIsFavorite()

        collectionView.isScrollEnabled = false

        if let url = imgURL {
            print(url)
            detailImgView.hnk_setImageFromURL(url)
            viewMap()
        }

        headerImgHeight.constant = self.view.bounds.size.width

//        headerSearchingLabel.text = imgSearchSpotName
//        headerSpotLabel.text = imgName

        printlatlon()
    }

    func initIsFavorite() {
        let q = NCMBQuery(className: "userFavorite")
        q?.whereKey("userId", equalTo: appDelegate.id)
        q?.whereKey("imgUrl", equalTo: self.imgURL?.absoluteString)
        q?.whereKey("deleteFlag", equalTo: true)
        q?.findObjectsInBackground({(objects, error) in
            if (error != nil){
                print("検索失敗")
            }else{
                // 検索成功時の処理
                print(objects! as! [NCMBObject]) // (例)検索結果を表示する
                if objects!.count == 0 {
                    print("中身ないよ")
                    self.favoriteBtn.backgroundColor = UIColor.clear
                    self.isFavorite = false
                } else {
                    let obj = objects! as! [NCMBObject]
                    self.isFavorite = obj[0].object(forKey: "deleteFlag") as! Bool
                    self.favoriteBtn.backgroundColor = UIColor.cyan
                }
            }
        })
    }

    func viewMap() {
        if let la = imgLat {
            if let lo = imgLon {
                if let name = imgName {

                    // ズームレベル.
                    let zoom: Float = 16

                    // カメラを生成.
                    let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: la,longitude: lo, zoom: zoom)

                    // MapViewを生成.
                    googleMap = GMSMapView(frame: CGRect(x:0, y:0, width:self.view.bounds.size.width, height:self.mapDisplayView.bounds.size.height))

                    // MapViewにカメラを追加.
                    googleMap.camera = camera

                    //マーカーの作成
                    let marker: GMSMarker = GMSMarker()
                    marker.position = CLLocationCoordinate2DMake(la, lo)
                    marker.map = googleMap


                    //viewにMapViewを追加.
                    mapDisplayView.addSubview(googleMap)
                }
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func printlatlon() {
        print("\(self.imgLat!), \(self.imgLon!)")

        //instagramAPI
        urlString = "https://api.instagram.com/v1/media/search?access_token=\(self.accessToken)&lat=\(self.imgLat!)&lng=\(self.imgLon!)&distance=0.01"

        //日本語を含むURLのエンコード
        encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!

        let req = Alamofire.request(encodedUrl, parameters: param)
        print(req)

        req.responseJSON { response -> Void in
            if let dict = response.result.value as? NSDictionary {

                let json = JSON(dictionary: dict)

                self.getInstagramData(dictionary: dict)
                self.nc.addObserver(self, selector: #selector(self.setHeight), name: self.myNotification, object: nil)
                self.collectionView.reloadData()

            }

        }

    }

    //インスタグラムから必要な情報を取得
    func getInstagramData(dictionary: NSDictionary) {
        let json = JSON(dictionary: dictionary)
        print(json)
        print("--------------------")

        //ハッシュタグで取得したデータリストから構造体項目取得
        if let array = json["data"].array {
            for d in array {
                let num = d["carousel_media"].count

                //複数画像取得
                if num != 0 {
                    for i in 0..<num {
                        sameSpots.append(d["carousel_media"][i]["images"]["thumbnail"]["url"].url! as NSURL)
                    }
                //画像一枚取得
                } else {
                    sameSpots.append(d["images"]["thumbnail"]["url"].url! as NSURL)
                }
            }
        }
    }


    func setHeight(){

        //行数の計算
        var lineNum = sameSpots.count / 3
        if sameSpots.count % 3 != 0 {
            lineNum += 1
        }

        //collectionViewの高さ計算（行間に隙間なし）
        let viewHeight = (CGFloat(lineNum) * cellSize)

        //collectionViewの高さ変更
        collectionHeight.constant = CGFloat(viewHeight)
    }

    // Screenサイズに応じたセルサイズを返す
    // UICollectionViewDelegateFlowLayoutの設定が必要
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        cellSize = self.view.frame.size.width/3-2

        self.nc.post(name: self.myNotification,object: nil)

        // 正方形で返すためにwidth,heightを同じにする
        return CGSize(width: cellSize, height: cellSize)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sameSpots.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sameSpotCell", for: indexPath)  as! detailViewCell

        print(sameSpots)

        cell.sameSpotImgView.hnk_setImageFromURL(self.sameSpots[indexPath.row] as URL)
        return cell
    }

    func tapFavorite() {

        //self.nc.addObserver(self, selector: #selector(self.dbOperate), name: self.dbNotification, object: nil)

        let q = NCMBQuery(className: "userFavorite")
        q?.whereKey("userId", equalTo: appDelegate.id)
        q?.whereKey("imgUrl", equalTo: self.imgURL?.absoluteString)
        q?.findObjectsInBackground({(objects, error) in
            if (error != nil){
                print("検索失敗")
            }else{
                // 検索成功時の処理
                print(objects! as! [NCMBObject]) // (例)検索結果を表示する
                if objects!.count == 0 {
                    print("中身ないよ")
                    self.favoriteBtn.backgroundColor = UIColor.clear
                    self.isFavorite = false
                    self.objTemp = objects! as! [NCMBObject]
                } else {
                    let obj = objects! as! [NCMBObject]
                    self.objTemp = objects! as! [NCMBObject]
                    self.isFavorite = obj[0].object(forKey: "deleteFlag") as! Bool
                    self.favoriteBtn.backgroundColor = UIColor.cyan

                }
                if !self.isFavorite {
                    if self.objTemp.count == 0 {
                        self.obj?.setObject(self.appDelegate.id, forKey: "userId")
                        self.obj?.setObject(self.imgName, forKey: "imgName")
                        self.obj?.setObject(self.imgSearchSpotName, forKey: "imgSearchSpotName")
                        self.obj?.setObject(self.imgURL!.absoluteString, forKey: "imgUrl")
                        self.obj?.setObject(self.imgLat, forKey: "lat")
                        self.obj?.setObject(self.imgLon, forKey: "lon")
                        self.obj?.setObject(true, forKey: "deleteFlag")

                        self.obj?.saveInBackground( { (error) in
                            if error != nil {
                                print("保存エラー")
                            } else {
                                print("保存成功")
                                self.isFavorite = true
                                print("favorite")
                                self.favoriteBtn.backgroundColor = UIColor.cyan
                            }
                        })
                    } else {
                        self.objTemp[0].setObject(true, forKey: "deleteFlag")
                        self.isFavorite = true
                        self.favoriteBtn.backgroundColor = UIColor.cyan
                        print("favorite")
                        self.objTemp[0].saveInBackground({(error) in
                            if error != nil {print("Save error : ",error!)}
                        })
                    }

                } else {
                    self.objTemp[0].setObject(false, forKey: "deleteFlag")
                    self.isFavorite = false
                    self.favoriteBtn.backgroundColor = UIColor.clear
                    print("clear favorite")
                    self.objTemp[0].saveInBackground({(error) in
                        if error != nil {print("Save error : ",error!)}
                    })

                }
            }
        })

    }

    func dbOperate() {

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */



}
