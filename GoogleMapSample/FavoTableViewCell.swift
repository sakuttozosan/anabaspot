//
//  FavoTableViewCell.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/11/17.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit

class FavoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var favoImg: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var rightLabel: UILabel!
    
}

