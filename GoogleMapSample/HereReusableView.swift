//
//  HereReusableView.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/08/11.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit

class HereReusableView: UICollectionReusableView {
        
    @IBOutlet weak var mapDisplayView: UIView!
}
