//
//  TableLoadViewController.swift
//  GoogleMapSample
//
//  Created by 澤入圭佑 on 2017/08/10.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoadLineViewController: UIViewController {
    
    let nc = NotificationCenter.default
    
    let myNotification = Notification.Name(rawValue: "getLine")
    
    var lines:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
//        getLine()
//        
//        nc.addObserver(self, selector: #selector(self.nextScene), name: myNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getLine()
        
        nc.addObserver(self, selector: #selector(self.nextScene), name: myNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getLine() {
        
        // URLを指定
        let stringUrl = "http://express.heartrails.com/api/json?method=getLines&prefecture=東京都"
        //    県名取得
        //    let stringUrl = "http://geoapi.heartrails.com/api/json?method=getPrefectures"
        //リクエスト作成
        let requestUrl = stringUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        let url = URL(string: requestUrl!)
        let request = URLRequest(url: url!)
        
        
        // コンフィグを指定してHTTPセッションを生成
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
        
        // HTTP通信を実行する
        // ※dataにJSONデータが入る
        let task:URLSessionDataTask = session.dataTask(with: request, completionHandler: {data, responce, error in
            // エラーがあったら出力
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                // データ取得後の処理
                // JSONデータを食わせる
                let json = JSON(data: data!)
                
                for line in json["response"]["line"].arrayValue {
                    self.lines.append(line.stringValue)
                }
                
                self.nc.post(name: self.myNotification,object: nil)
                
            }
            
        })
        
        // HTTP通信を実行
        task.resume()
    }
    
    func nextScene(notification: NSNotification) {
        performSegue(withIdentifier: "toLineTable",sender: nil)
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "toLineTable") {
            let lineVC: LineTableViewController = (segue.destination as? LineTableViewController)!
            // StationTableViewController のlineに選択された路線を設定する
            lineVC.lines = lines
            
            nc.removeObserver(self, name: self.myNotification, object: nil)
        }
    }
    
    
}
