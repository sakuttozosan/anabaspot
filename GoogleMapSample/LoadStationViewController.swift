//
//  TableLoadViewController2.swift
//  GoogleMapSample
//
//  Created by 澤入圭佑 on 2017/08/10.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit

import UIKit
import SwiftyJSON

class LoadStationViewController: UIViewController {
    
    let nc = NotificationCenter.default
    
    let myNotification = Notification.Name(rawValue:"getStation")
    
    var line:String = ""
    
    var stations:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getStation()
        
        nc.addObserver(self, selector: #selector(self.nextScene), name: myNotification, object: nil)

        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getStation() {
        // URLを指定してオブジェクトを作成
        let stringUrl = "http://express.heartrails.com/api/json?method=getStations&line="+line
        //リクエスト作成
        let requestUrl = stringUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        let url = URL(string: requestUrl!)
        let request = URLRequest(url: url!)
        
        // コンフィグを指定してHTTPセッションを生成
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: OperationQueue.main)
        
        // HTTP通信を実行する
        // ※dataにJSONデータが入る
        let task:URLSessionDataTask = session.dataTask(with: request, completionHandler: {data, responce, error in
            // エラーがあったら出力
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                // データ取得後の処理
                // JSONデータを食わせる
                let json = JSON(data: data!)
                
                
                for station in json["response"]["station"].arrayValue {
                    self.stations.append(station["name"].stringValue)
                }
                
                self.nc.post(name: self.myNotification,object: nil)
                
            }
            
        })
        
        // HTTP通信を実行
        task.resume()
    }
    
    func nextScene(notification: NSNotification) {
        performSegue(withIdentifier: "toStationTable",sender: nil)
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "toStationTable") {
            let stationVC: StationTableViewController = (segue.destination as? StationTableViewController)!
            // StationTableViewController のlineに選択された路線を設定する
            stationVC.line = line
            stationVC.stations = stations
            
            nc.removeObserver(self, name: self.myNotification, object: nil)
        }
    }
    
}
