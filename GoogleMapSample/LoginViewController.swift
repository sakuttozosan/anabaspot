//
//  LoginViewController.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/09/10.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import WebKit
import NCMB

class LoginViewController: UIViewController, WKNavigationDelegate{

    //セキュリティのため非表示
    let client_id: String = "xxxxx"
    let redirectUrl = "http://instagramtest.com/instagram/"
    let secretId = "xxxxx"
    var code: String? = nil

    var userData: Dictionary<String, AnyObject>? = nil

    let nc = NotificationCenter.default
    let myNotification = Notification.Name(rawValue: "getUser")
    let dbNotification = Notification.Name(rawValue: "dbOperate")

    var appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
    //ユーザー構造体
    struct User {
        var id: String?
        var userName: String?
        var thumbnailURL: String?
        var accessToken: String?
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        let instagramAuthUrl = "https://api.instagram.com/oauth/authorize/?client_id=\(client_id)&redirect_uri=\(redirectUrl)&response_type=code&scope=basic+public_content"

        let instagramWebView = WKWebView()
        let requestURL = NSURL(string: instagramAuthUrl)
        let request = NSURLRequest(url: requestURL! as URL)


        instagramWebView.load(request as URLRequest)
        instagramWebView.navigationDelegate = self
        view = instagramWebView
        self.nc.addObserver(self, selector: #selector(self.getUser), name: self.myNotification, object: nil)
        self.nc.addObserver(self, selector: #selector(self.dbOperate), name: self.dbNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
        let redirectURL = "http://instagramtest.com/instagram/"
        let URL = navigationAction.request.url?.absoluteString


        if Regexp(redirectURL).isMatch(URL ?? "") {
            let comp = NSURLComponents(string: URL!)
            let code = comp?.queryItems?[0].value
            // codeが取れているか確認する
            if code != nil {
                if code != self.client_id {
                    print("---code---")
                    print(code ?? "")
                    self.code = code
                    self.nc.post(name: self.myNotification, object: nil)
                } else {
                    print("クライアントIdと一緒")
                }
            } else {
                print("code値を取得できていません")
            }


        }
    }

    func getUser() {
        let urlString = "https://api.instagram.com/oauth/access_token"
        let url = NSURL(string: urlString)!
        let paramString  = "client_id=\(self.client_id)&client_secret=\(self.secretId)&grant_type=authorization_code&redirect_uri=\(self.redirectUrl)&code=\(self.code!)&scope=basic+public_content"

        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = paramString.data(using: String.Encoding.utf8)!

        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        self.userData = jsonDataDict
                        NSLog("Received data:\n\(self.userData))")
                        self.nc.post(name: self.dbNotification, object: nil)
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }

        task.resume()
    }

    func dbOperate() {
        let obj = NCMBObject(className: "instaUser")

        appDelegate.id = self.userData?["user"]?["id"] as? String
        appDelegate.userName = self.userData?["user"]?["username"] as? String
        appDelegate.thumbnailURL = self.userData?["user"]?["profile_picture"] as? String
        appDelegate.accessToken = self.userData?["access_token"] as? String

        //ローカルにuser情報保存
        UserDefaults.standard.set(appDelegate.id, forKey: "ID")
        UserDefaults.standard.set(appDelegate.userName, forKey: "USERNAME")
        UserDefaults.standard.set(appDelegate.thumbnailURL, forKey: "THUMBNAILURL")
        UserDefaults.standard.set(appDelegate.accessToken, forKey: "ACCESSTOKEN")

        var user = User()
        user.id = self.userData?["user"]?["id"] as? String
        user.userName = self.userData?["user"]?["username"] as? String
        user.thumbnailURL = self.userData?["user"]?["profile_picture"] as? String
        user.accessToken = self.userData?["access_token"] as? String

        let query = NCMBQuery(className: "instaUser")
        query?.whereKey("id", equalTo: user.id)
        query?.findObjectsInBackground({(objects, error) in
            if (error != nil){
                print("検索失敗")
            }else{
                // 検索成功時の処理
                print(objects! as! [NCMBObject]) // (例)検索結果を表示する
                if objects!.count == 0 {
                    print("中身ないよ")
                    obj?.setObject(user.id, forKey: "id")
                    obj?.setObject(user.userName, forKey: "userName")
                    obj?.setObject(user.thumbnailURL, forKey: "thumbnailURL")
                    obj?.setObject(user.accessToken, forKey: "accessToken")

                    obj?.saveInBackground( { (error) in
                        if error != nil {
                            print("保存エラー")
                        } else {
                            print("保存成功")
                            let storyboard: UIStoryboard = self.storyboard!
                            let nextView = storyboard.instantiateViewController(withIdentifier: "home") as! TabController
                            self.present(nextView, animated: true, completion: nil)
                        }
                    })
                } else {
                    print("既存ユーザーだよ")
                    let storyboard: UIStoryboard = self.storyboard!
                    let nextView = storyboard.instantiateViewController(withIdentifier: "home") as! TabController
                    self.present(nextView, animated: true, completion: nil)
                }
            }
        })
    }
}


class Regexp {

    let regexp: NSRegularExpression
    let pattern: String

    init(_ pattern: String) {
        self.pattern = pattern

        do {
            self.regexp = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
        } catch let error as NSError {
            print(error)
            self.regexp = NSRegularExpression()
        }
    }

    func isMatch(_ input: String) -> Bool {
        let matches = regexp.matches( in: input, options: [], range:NSMakeRange(0, input.characters.count) )
        return matches.count > 0
    }

}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

