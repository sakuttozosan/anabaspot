//
//  MyPageViewController.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/10/08.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import Haneke
import NCMB

class MyPageViewController: UIViewController,UITabBarDelegate, UITableViewDelegate, UITableViewDataSource {

    //データベース
    let obj = NCMBObject(className: "userFavorite")

    //お気に入り画像更新
    var refreshControl:UIRefreshControl!
    let nc = NotificationCenter.default

    var byDistance: [NCMBObject] = []

    var byDate: [NCMBObject] = []

    @IBOutlet weak var favoriteTable: UITableView!
    @IBOutlet weak var myPageTab: UITabBar!
    @IBOutlet weak var firstSelected: UITabBarItem!
    @IBOutlet weak var profileImageView: UIImageView!
    var appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate

    var profileUrl: URL? = nil

    var selectedData:[NCMBObject]!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.nc.addObserver(self, selector: #selector(self.getFavoImage), name: NSNotification.Name("reloadImg"), object: nil)

        myPageTab.delegate = self

        myPageTab.selectedItem = firstSelected

        //お気に入りした画像取得
        getFavoImage()

        //table初期値
        selectedData = byDate

        //データ反映
        favoriteTable.reloadData()


        if let url = appDelegate.thumbnailURL {
            profileUrl = URL(string: url)!
            profileImageView.hnk_setImageFromURL(profileUrl!)

            // 画面の横幅を取得
            let screenWidth:CGFloat = view.frame.size.width
            let screenHeight:CGFloat = view.frame.size.height

            // 画像の幅・高さの取得
            let imgWidth = profileImageView.frame.width
            let imgHeight = profileImageView.frame.height

            // 画像サイズをスクリーン幅に合わせる
            let scale = screenWidth / imgWidth * 0.5
            let rect:CGRect = CGRect(x:0, y:0, width:imgWidth*scale, height:imgHeight*scale)

            // ImageView frame をCGRectで作った矩形に合わせる
            profileImageView.frame = rect;

            // 画像の中心を画面の中心に設定
            profileImageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
            // 角丸にする
            profileImageView.layer.cornerRadius = profileImageView.frame.size.width * 0.8
            profileImageView.clipsToBounds = true

            // UIImageViewのインスタンスをビューに追加
            self.view.addSubview(profileImageView)

        }

        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "お気に入り画像を更新")
        self.refreshControl.addTarget(self, action: #selector(MyPageViewController.refresh), for: UIControlEvents.valueChanged)
        self.favoriteTable.addSubview(refreshControl)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    public func getFavoImage(){
        //ユーザーのお気に入りした画像を取得
        let q = NCMBQuery(className: "userFavorite")
        q?.whereKey("userId", equalTo: appDelegate.id)
        q?.whereKey("deleteFlag", equalTo: true)
        q?.addDescendingOrder("updateDate")
        //新しい順に取得
        //q?.addAscendingOrder("updateDate")
        q?.findObjectsInBackground({(objects, error) in
            if (error != nil){
                print("検索失敗")
            }else{
                // 検索成功時の処理
                print("++++++++++++++")
                print(objects! as! [NCMBObject]) // (例)検索結果を表示する
                print("++++++++++++++")
                if objects!.count == 0 {
                    print("中身ないよ")
                } else {
                    let obj = objects! as! [NCMBObject]
                    self.byDate = obj
                    self.selectedData = self.byDate
                    self.byDistance = Array(self.byDate.reversed())
                    self.favoriteTable.reloadData()
                    self.nc.post(name: NSNotification.Name("reload"), object: nil)

                }
            }
        })
    }

    public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if(tabBar.tag == 1) {
            switch(item.tag) {
            case 0:
                print("byData : \(byDate)")
                selectedData = byDate
            case 1:
                print("byDistance : \(byDistance)")
                selectedData = byDistance
            default:
                break;
            }
            favoriteTable.reloadData()
        }
    }

    // セルの個数を指定するデリゲートメソッド（必須）
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedData.count
    }

    // セルに値を設定するデータソースメソッド（必須）
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // セルを取得する
        let cell: FavoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "favoriteCell", for: indexPath as IndexPath) as! FavoTableViewCell

        // セルに表示する値を設定する
        //cell.textLabel!.text = selectedData[indexPath.row].object(forKey: "imgName") as? String

        cell.rightLabel.text = ((selectedData[indexPath.row].object(forKey: "updateDate") as? String)?.components(separatedBy: "T")[0])?.replacingOccurrences(of: "-", with: "/")
        cell.nameLabel.text = selectedData[indexPath.row].object(forKey: "imgName") as? String
        cell.favoImg.hnk_setImageFromURL(URL(string: selectedData[indexPath.row].object(forKey: "imgUrl") as! String)!)

        return cell
    }

    // セルが選択された時に呼ばれるデリゲートメソッド
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }

    //スクロール画像リフレッシュ
    func refresh() {
        self.favoriteTable.reloadData()
//        self.selectedData = []
//        self.byDate = []
//        self.byDistance = []
        self.nc.post(name: NSNotification.Name("reloadImg"), object: nil)
        self.nc.addObserver(self, selector: #selector(self.stopRefresh), name: NSNotification.Name("reload"), object: nil)
    }

    //リフレッシュ完了
    func stopRefresh() {
        refreshControl.endRefreshing()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
