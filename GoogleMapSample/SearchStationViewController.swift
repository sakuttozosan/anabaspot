//
//  SearchStationViewController.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/08/02.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import Haneke

class SearchStationViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var refreshControl:UIRefreshControl!

    @IBOutlet weak var searchStationTitle: UINavigationItem!
    @IBOutlet var collectionView:UICollectionView!

    var appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

//    let accessToken: String = "1591420171.b8d896e.3e312489877d46cb9ec3c4884fed011e"
    var accessToken: String = ""
    //セキュリティのため非表示
    let param = ["client_id": "xxxxx"]

    var searchLine:String = ""
    var searchStation: String = ""
    var searchLatitude: CLLocationDegrees = 0.0
    var searchLongitude: CLLocationDegrees = 0.0

    var searchName: String = ""
    var urlString: String = ""
    var encodedUrl: String = ""



    /** インスタグラムからの取得情報構造体 */
    //取得したメディアのリスト
    var mediaList: [Media] = []
    struct Caption {
        var userName: String?
        var text: String?
    }

    struct Location {
        var latitude: Double?
        var longitude: Double?
        var name: String?
    }

    struct Media {
        var thumbnailURL: [NSURL]?
        var imageURL: [NSURL]?
        var location: Location?
        var caption: Caption?
    }

    let nc = NotificationCenter.default
    var oneMedia: Media?
    var imgURL: URL?

//    var imgURL: AnyObject? {
//        get {
//            return UserDefaults.standard.object(forKey: "imgURL") as? AnyObject
//        }
//        set {
//            UserDefaults.standard.set(imgURL!, forKey: "imgURL")
//            UserDefaults.standard.synchronize()
//        }
//
//    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        accessToken = appDelegate.accessToken!

        searchStation += "駅"
        searchStationTitle.title = searchStation


        //緯度経度取得
        getLatLon()
        //緯度経度処理待ち登録
        nc.addObserver(self, selector: #selector(self.printlatlon), name: NSNotification.Name("getLatLon"), object: nil)

        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "画像を更新")
        self.refreshControl.addTarget(self, action: #selector(SearchViewController.refresh), for: UIControlEvents.valueChanged)
        self.collectionView.addSubview(refreshControl)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func printlatlon(notification: Notification?) {
        print("\(self.searchLatitude), \(self.searchLongitude)")

        //instagramAPI
        urlString = "https://api.instagram.com/v1/media/search?access_token=\(self.accessToken)&lat=\(self.searchLatitude)&lng=\(self.searchLongitude)"

        //日本語を含むURLのエンコード
        encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!

        let req = Alamofire.request(encodedUrl, parameters: param)
        print(req)

        req.responseJSON { response -> Void in
            if let dict = response.result.value as? NSDictionary {

                self.getInstagramData(dictionary: dict)
                self.collectionView.reloadData()

            }

        }

    }

    //インスタグラムから必要な情報を取得
    func getInstagramData(dictionary: NSDictionary) {
        let json = JSON(dictionary: dictionary)
        //print(json)
        print("--------------------")

        //ハッシュタグで取得したデータリストから構造体項目取得
        if let array = json["data"].array {
            for d in array {
                print(d)
                let num = d["carousel_media"].count

                print(num)
                var imageUrlList: [NSURL]? = []
                var thumbnailUrlList: [NSURL]? = []

                var caption = Caption(
                    userName: d["caption"]["from"]["username"].string!,
                    text: d["caption"]["text"].string!
                )

                //複数画像取得
                if num != 0 {
                    for i in 0..<num {
                        imageUrlList?.append(d["carousel_media"][i]["images"]["standard_resolution"]["url"].url! as NSURL)
                        thumbnailUrlList?.append(d["carousel_media"][i]["images"]["thumbnail"]["url"].url! as NSURL)
                    }
                    //画像一枚取得
                } else {
                    imageUrlList?.append(d["images"]["standard_resolution"]["url"].url! as NSURL)
                    thumbnailUrlList?.append(d["images"]["thumbnail"]["url"].url! as NSURL)
                }

                //場所取得
                var location: Location?
                if !d["location"].isEmpty {
                    location = Location(
                        latitude: d["location"]["latitude"].double!,
                        longitude: d["location"]["longitude"].double!,
                        name: d["location"]["name"].string!
                    )
                }

                var media = Media(
                    thumbnailURL: thumbnailUrlList,
                    imageURL: imageUrlList,
                    location: location,
                    caption: caption
                )
                self.mediaList.append(media)
            }
            print(self.mediaList)
            self.nc.post(name: NSNotification.Name("reload"), object: nil)
        }

    }

    func getLatLon(){
        let address = searchStation
        var latLonList: [CLLocationDegrees]? = []
        print(address)

        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placeMarks:[CLPlacemark]?, error: Error?) -> Void in
            for placeMark in placeMarks! {
                self.searchLatitude = (placeMark.location?.coordinate.latitude)!
                latLonList?.append(self.searchLatitude)
                self.searchLongitude = (placeMark.location?.coordinate.longitude)!
                latLonList?.append(self.searchLongitude)

                //処理解放送信
                self.nc.post(name: NSNotification.Name("getLatLon"), object: nil)

            }
        }
    }

    // Screenサイズに応じたセルサイズを返す
    // UICollectionViewDelegateFlowLayoutの設定が必要
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize:CGFloat = self.view.frame.size.width/3-2
        // 正方形で返すためにwidth,heightを同じにする
        return CGSize(width: cellSize, height: cellSize)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaList.count ?? 0

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)  as! anabaViewCell

        cell.imgView.hnk_setImageFromURL(self.mediaList[indexPath.row].imageURL?[0] as! URL)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        oneMedia = self.mediaList[indexPath.row] as Media

        //imgURL = oneMedia!.thumbnailURL?[0] as! URL

        if oneMedia != nil {
            performSegue(withIdentifier: "searchDetailImg", sender: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {
            return
        }
        if identifier == "searchDetailImg" {
            let detailVC: DetailViewController = (segue.destination as? DetailViewController)!
            detailVC.imgURL = oneMedia?.imageURL?[0] as? URL
            detailVC.imgLat = oneMedia?.location?.latitude
            detailVC.imgLon = oneMedia?.location?.longitude
            detailVC.imgName = oneMedia?.location?.name
            detailVC.imgSearchSpotName = searchStation

        }

    }

    //スクロール画像リフレッシュ
    func refresh() {
        self.collectionView.reloadData()
        self.mediaList = []
        self.nc.post(name: NSNotification.Name("getLatLon"), object: nil)
        self.nc.addObserver(self, selector: #selector(self.stopRefresh), name: NSNotification.Name("reload"), object: nil)
    }

    //リフレッシュ完了
    func stopRefresh() {
        refreshControl.endRefreshing()
    }





    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
