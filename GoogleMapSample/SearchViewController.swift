//
//  SearchViewController.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/07/31.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import SwiftyJSON
import Haneke
import CoreLocation
class SearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate {

    var refreshControl:UIRefreshControl!


    var mapDisplayView: UIView!
    var googleMap : GMSMapView!

    @IBOutlet weak var collectionView: UICollectionView!

    var appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
//    let accessToken: String = "1591420171.b8d896e.42bff6e6c6984e9cbd6f57dfcf8711f6"
    var accessToken: String = ""
    //セキュリティのため非表示
    let param = ["client_id": "xxxxx"]

    var urlString: String = ""
    var encodedUrl: String = ""

    var searchLatitude: CLLocationDegrees = 0.0
    var searchLongitude: CLLocationDegrees = 0.0

    var searchStation: String? = nil

    var lm: CLLocationManager!


    /** インスタグラムからの取得情報構造体 */
    //取得したメディアのリスト
    var mediaList: [Media] = []
    struct Caption {
        var userName: String?
        var text: String?
    }

    struct Location {
        var latitude: Double?
        var longitude: Double?
        var name: String?
    }

    struct Media {
        var thumbnailURL: [NSURL]?
        var imageURL: [NSURL]?
        var location: Location?
        var caption: Caption?
    }

    let nc = NotificationCenter.default
    var oneMedia: Media?

    override func viewDidLoad() {
        super.viewDidLoad()
        accessToken = appDelegate.accessToken!

        // Do any additional setup after loading the view.

        lm = CLLocationManager()
        lm.delegate = self

        nc.addObserver(self, selector: #selector(self.viewMap), name: NSNotification.Name("getMapView"), object: nil)

        nc.addObserver(self, selector: #selector(self.printLatLon), name: NSNotification.Name("printLatLon"), object: nil)

        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "画像を更新")
        self.refreshControl.addTarget(self, action: #selector(SearchViewController.refresh), for: UIControlEvents.valueChanged)
        self.collectionView.addSubview(refreshControl)

        // セキュリティ認証のステータスを取得
        let status = CLLocationManager.authorizationStatus()

        //まだ認証が得られていない場合は、認証ダイアログを表示
        if status == CLAuthorizationStatus.notDetermined {
            print("didChangeAuthorizationStatus:\(status)");
            // まだ承認が得られていない場合は、認証ダイアログを表示
            self.lm.requestAlwaysAuthorization()
        }

        // 取得精度の設定
        lm.desiredAccuracy = kCLLocationAccuracyBest
        // 取得頻度の設定
        lm.distanceFilter = 100

        lm.requestLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations location: [CLLocation]){

        // 取得した緯度がnewLocation.coordinate.longitudeに格納されている
        self.searchLatitude = (manager.location?.coordinate.latitude)!
        // 取得した経度がnewLocation.coordinate.longitudeに格納されている
        self.searchLongitude = (manager.location?.coordinate.longitude)!
        // 取得した緯度・経度をLogに表示
        print("updateLocation : latitude: \(self.searchLatitude) , longitude: \(self.searchLongitude)")

        self.nc.post(name: NSNotification.Name("printLatLon"), object: nil)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // この例ではLogにErrorと表示するだけ．
        print("Error")
    }

    func viewMap(notification: Notification?) {
        // ズームレベル.
        let zoom: Float = 16

        // カメラを生成.
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: searchLatitude,longitude: searchLongitude, zoom: zoom)

        // MapViewを生成.
        googleMap = GMSMapView(frame: CGRect(x:0, y:0, width:self.view.bounds.size.width, height:mapDisplayView.bounds.height))

        // MapViewにカメラを追加.
        googleMap.camera = camera

        //マーカーの作成
        let marker: GMSMarker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(searchLatitude, searchLongitude)
        marker.map = googleMap


        //viewにMapViewを追加.
        mapDisplayView.addSubview(googleMap)

    }

    //インスタグラムから必要な情報を取得
    func getInstagramData(dictionary: NSDictionary) {
        let json = JSON(dictionary: dictionary)
        //print(json)
        print("--------------------")

        //ハッシュタグで取得したデータリストから構造体項目取得
        if let array = json["data"].array {
            for d in array {
                print(d)
                let num = d["carousel_media"].count

                print(num)
                var imageUrlList: [NSURL]? = []
                var thumbnailUrlList: [NSURL]? = []

                var caption = Caption(
                    userName: d["caption"]["from"]["username"].string!,
                    text: d["caption"]["text"].string!
                )

                //複数画像取得
                if num != 0 {
                    for i in 0..<num {
                        imageUrlList?.append(d["carousel_media"][i]["images"]["standard_resolution"]["url"].url! as NSURL)
                        thumbnailUrlList?.append(d["carousel_media"][i]["images"]["thumbnail"]["url"].url! as NSURL)
                    }
                    //画像一枚取得
                } else {
                    imageUrlList?.append(d["images"]["standard_resolution"]["url"].url! as NSURL)
                    thumbnailUrlList?.append(d["images"]["thumbnail"]["url"].url! as NSURL)
                }

                //場所取得
                var location: Location?
                if !d["location"].isEmpty {
                    location = Location(
                        latitude: d["location"]["latitude"].double!,
                        longitude: d["location"]["longitude"].double!,
                        name: d["location"]["name"].string!
                    )
                }

                var media = Media(
                    thumbnailURL: thumbnailUrlList,
                    imageURL: imageUrlList,
                    location: location,
                    caption: caption
                )
                self.mediaList.append(media)
            }
            print(self.mediaList)
            self.nc.post(name: NSNotification.Name("reload"), object: nil)
        }

    }

    func printLatLon() {
        //instagramAPI
        urlString = "https://api.instagram.com/v1/media/search?access_token=\(self.accessToken)&lat=\(self.searchLatitude)&lng=\(self.searchLongitude)"

        //日本語を含むURLのエンコード
        encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!

        let req = Alamofire.request(encodedUrl, parameters: param)
        print(req)

        req.responseJSON { response -> Void in
            if let dict = response.result.value as? NSDictionary {

                self.getInstagramData(dictionary: dict)
                self.collectionView.reloadData()

            }

        }
    }

    // Screenサイズに応じたセルサイズを返す
    // UICollectionViewDelegateFlowLayoutの設定が必要
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize:CGFloat = collectionView.frame.size.width/3-2
        // 正方形で返すためにwidth,heightを同じにする
        return CGSize(width: cellSize, height: cellSize)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaList.count ?? 0

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hereCell", for: indexPath)  as! anabaHereViewCell


        cell.imgHereView.hnk_setImageFromURL(self.mediaList[indexPath.row].imageURL?[0] as! URL)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        let sec = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "map", for: indexPath) as! HereReusableView

        self.mapDisplayView = sec.mapDisplayView

        //処理解放送信
        self.nc.post(name: NSNotification.Name("getMapView"), object: nil)

        return sec
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        oneMedia = self.mediaList[indexPath.row] as Media

        //imgURL = oneMedia!.thumbnailURL?[0] as! URL

        if oneMedia != nil {
            performSegue(withIdentifier: "searchHereDetailImg", sender: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {
            return
        }
        if identifier == "searchHereDetailImg" {
            let detailVC: DetailViewController = (segue.destination as? DetailViewController)!
            detailVC.imgURL = oneMedia?.imageURL?[0] as? URL
            detailVC.imgLat = oneMedia?.location?.latitude
            detailVC.imgLon = oneMedia?.location?.longitude
            detailVC.imgName = oneMedia?.location?.name
            detailVC.imgSearchSpotName = "現在地周辺"

        }

    }


    //スクロール画像リフレッシュ
    func refresh() {
        self.collectionView.reloadData()
        self.mediaList = []
        self.printLatLon()
        self.nc.addObserver(self, selector: #selector(self.stopRefresh), name: NSNotification.Name("reload"), object: nil)
    }

    //リフレッシュ完了
    func stopRefresh() {
        refreshControl.endRefreshing()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
