//
//  TabController.swift
//  GoogleMapSample
//
//  Created by 澤入圭佑 on 2017/08/02.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit

class TabController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //タブの背景色
        UITabBar.appearance().barTintColor = UIColor.lightGray
        
        // タブバーアイコン選択時の色を変更（iOS 9以前でも利用可能）
        UITabBar.appearance().tintColor = UIColor.blue
        
        // タブバーアイコン非選択時の色を変更（iOS 10で利用可能）
        UITabBar.appearance().unselectedItemTintColor = UIColor.black
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
