//
//  ViewController.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/07/30.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {

    var googleMap : GMSMapView!
    var lm: CLLocationManager!
    var latitude: CLLocationDegrees = 0.0
    var longitude: CLLocationDegrees = 0.0



    //緯度経度 -> 金沢駅
//    let latitude: CLLocationDegrees = 36.5780574
//    let longitude: CLLocationDegrees = 136.6486596

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        lm = CLLocationManager()
        lm.delegate = self

        // セキュリティ認証のステータスを取得
        let status = CLLocationManager.authorizationStatus()

         //まだ認証が得られていない場合は、認証ダイアログを表示
        if status == CLAuthorizationStatus.notDetermined {
            print("didChangeAuthorizationStatus:\(status)");
            // まだ承認が得られていない場合は、認証ダイアログを表示
            self.lm.requestAlwaysAuthorization()
        }

        // 取得精度の設定
        lm.desiredAccuracy = kCLLocationAccuracyBest
        // 取得頻度の設定
        lm.distanceFilter = 100

        lm.startUpdatingLocation()


    }



    func locationManager(_ manager: CLLocationManager, didUpdateLocations location: [CLLocation]){
        // 取得した緯度がnewLocation.coordinate.longitudeに格納されている
        self.latitude = (manager.location?.coordinate.latitude)!
        // 取得した経度がnewLocation.coordinate.longitudeに格納されている
        self.longitude = (manager.location?.coordinate.longitude)!
        // 取得した緯度・経度をLogに表示
        //print("latiitude: \(self.latitude) , longitude: \(self.longitude)")
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // この例ではLogにErrorと表示するだけ．
        print("Error")
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //セグエの処理
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {
            return
        }
        if identifier == "YouAreHere" {
//            lm.startUpdatingLocation()
            let resultVC = segue.destination as! SearchViewController
            resultVC.searchLatitude = self.latitude
            resultVC.searchLongitude = self.longitude

        }

    }




}

