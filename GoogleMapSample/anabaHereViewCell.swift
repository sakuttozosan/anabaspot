//
//  anabaHereViewCell.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/08/08.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit

class anabaHereViewCell: UICollectionViewCell {
    
    //現在地周辺から取得した画像一覧表示用
    @IBOutlet weak var imgHereView: UIImageView!
}
