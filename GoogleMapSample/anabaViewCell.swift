//
//  anabaViewCell.swift
//  GoogleMapSample
//
//  Created by 吉野貴大 on 2017/08/08.
//  Copyright © 2017年 吉野貴大. All rights reserved.
//

import UIKit

class anabaViewCell: UICollectionViewCell {
    
    //指定した駅周辺から取得した画像一覧表示用
    @IBOutlet weak var imgView: UIImageView!
}
